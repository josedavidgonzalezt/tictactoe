import Square from './Square'
const Board = ({ squares, setSquares, onClick, next, winner, setNext }) => { 
  const reset = () =>{
    setNext(true)
    setSquares(Array(9).fill(null))
  }

  return(
    <div className="contendor">
      <div className="player">Next Player: {next ? 'X' : 'O'}</div>
      <div className={`winner ${winner!== null ? 'show-w':''}`}>Winner: {winner}</div>
      <div className="board-row">
        <Square value={squares[0]} onClick={() => onClick(0)} />
        <Square value={squares[1]} onClick={() => onClick(1)} />
        <Square value={squares[2]} onClick={() => onClick(2)} />
        <Square value={squares[3]} onClick={() => onClick(3)} />
        <Square value={squares[4]} onClick={() => onClick(4)} />
        <Square value={squares[5]} onClick={() => onClick(5)} />
        <Square value={squares[6]} onClick={() => onClick(6)} />
        <Square value={squares[7]} onClick={() => onClick(7)} />
        <Square value={squares[8]} onClick={() => onClick(8)} />
      </div>
      <div>
        <button
          className="reset"
          onClick={()=> reset()}
          disabled={winner === null & true}
        >Reset</button>
      </div>
  </div>
  )
}
export default Board;